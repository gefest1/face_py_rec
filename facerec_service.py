from os import listdir, remove
from os.path import isfile, join, splitext

import face_recognition
from flask import Flask, jsonify, request
from flask_cors import CORS
from werkzeug.exceptions import BadRequest

# Create flask app
app = Flask(__name__)
CORS(app)

# <Picture functions> #


def is_picture(filename):
    image_extensions = {'png', 'jpg', 'jpeg', 'gif'}
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in image_extensions


def calc_face_encoding(image):
    # Currently only use first face found on picture
    loaded_image = face_recognition.load_image_file(image)
    faces = face_recognition.face_encodings(loaded_image)

    # If more than one face on the given image was found -> error
    if len(faces) > 1:
        raise Exception(
            "Found more than one face in the given training image.")

    # If none face on the given image was found -> error
    if not faces:
        raise Exception("Could not find any face in the given training image.")

    return faces[0]


def tuTrueOrFalse(dd):
    if (dd):
        return 1
    return 0


def detect_faces_in_image(file_stream, filestreams):
    # Load the uploaded image file
    print("START FACE REC")
    img = face_recognition.load_image_file(file_stream)

    # Get face encodings for any faces in the uploaded image
    uploaded_faces = face_recognition.face_encodings(img)

    imgs = [(calc_face_encoding(image))
            for image in filestreams]

    # Defaults for the result object
    faces_found = len(uploaded_faces)
    faces = []
    if (faces_found) != 1:
        raise Exception(
            "Wrong count of faces")
    if len(faces) > 1:
        raise Exception(
            "Found more than one face in the given training image.")
    uploaded_face = uploaded_faces[0]

    match_results = face_recognition.compare_faces(
        imgs, uploaded_face)

    return {
        'match': list(map(tuTrueOrFalse, match_results))
    }


# <Picture functions> #

# <Controller>


@app.route('/', methods=['POST'])
def web_recognize():
    file = extract_image(request)
    files = extract_images(request)

    if file and is_picture(file.filename):
        # The image file seems valid! Detect faces and return the result.
        return jsonify(detect_faces_in_image(file, files))
    else:
        raise BadRequest("Given file is invalid!")


def extract_images(request):
    # Check if a valid image file was uploaded
    # request.files.getlist("files")
    files = request.files.getlist('files')
    if len(files) == 0:
        raise BadRequest("Missing file parameter!")

    return files


def extract_image(request):
    # Check if a valid image file was uploaded
    if 'file' not in request.files:
        raise BadRequest("Missing file parameter!")

    file = request.files['file']
    if file.filename == '':
        raise BadRequest("Given file is invalid")

    return file
# </Controller>


if __name__ == "__main__":

    print("Starting WebServer...")
    app.run(host='0.0.0.0', port=8080, debug=False)
# docker run -d --name py-face -p8080:8080 facerec_service 